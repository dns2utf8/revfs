#!/usr/bin/env bash

cargo build
cargo run --bin revfs_exporter -- . "$1"
