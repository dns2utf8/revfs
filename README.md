# refs

Reverse remote file system.

0. $ revfs ./local_path/ user@remote.machine
1. connect via ssh
2. establish back channel
    2.0 support ssh's -D
    2.1 support ssh's -L
    2.2 support ssh'2 -R
3. provide a remote shell
4. mobilize client
5. mobilize server

# Planed features

- [ ] establish ssh connection with
    - [ ] system ssh CLI
    - [ ] libssh2
- [ ] copy over current binary on the fly (ensure binary compatibility)
- [ ] export local file system
    - [ ] with inodes
    - [ ] with original uid / gid
    - [ ] on the fly translate uid / gid
- [ ] add mosh like resume
    - [ ] with direct AES encrypted channel
    - [ ] or any other mechanism
- [ ] add remote disk cache
    - [ ] encrypted disk cache
