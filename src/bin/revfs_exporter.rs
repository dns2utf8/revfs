#[macro_use]
extern crate serde_derive;

// Maybe later in the project
//pub mod aes_transport;

const USAGE: &'static str = "
The reverse sshfs local half. The program you use to initiate the mount.

Usage:
  revfs_exporter [options] <local-directory> <remote>
  revfs_exporter (-h | --help)
  revfs_exporter --version

Options:
  -h --help          Show this screen.
  --version          Show version.
  --soft             Auto unmount on remote if connection hangs
  --ignore-pattern   Files and folders matching this pattern will not get synced back to you
";

#[derive(Debug, Deserialize)]
struct Args {
    arg_local_directory: String,
    arg_remote: String,
    flag_soft: bool,
}

fn main() {
    let args: Args = docopt::Docopt::new(USAGE)
                            .and_then(|d| d.deserialize())
                            .unwrap_or_else(|e| e.exit());
    println!("{:?}", args);

    let target_dir = std::env::var("CARGO_TARGET_DIR").unwrap_or("target/".into());
    println!("target_dir: {}", target_dir);
}
