#[macro_use]
extern crate serde_derive;

// Maybe later in the project
//pub mod aes_transport;

const USAGE: &'static str = "
The reverse sshfs far half. This program you should use the revfs_exporter to initiate the export.

Usage:
  revfs_mounter [options]
  revfs_mounter (-h | --help)
  revfs_mounter --version

Options:
  -h --help          Show this screen.
  --version          Show version.
  --soft             Auto unmount on remote if connection hangs
  --ignore-pattern   Files and folders matching this pattern will not get synced back to you
";

#[derive(Debug, Deserialize)]
struct Args {
    flag_soft: bool,
}

fn main() {
    let args: Args = docopt::Docopt::new(USAGE)
                            .and_then(|d| d.deserialize())
                            .unwrap_or_else(|e| e.exit());
    println!("{:?}", args);

    println!("Hurra!");
}
