extern crate ring;

use self::ring::aead::{/*Algorithm, */OpeningKey, SealingKey, MAX_TAG_LEN, AES_256_GCM, open_in_place, seal_in_place};
use self::ring::rand::{SystemRandom, SecureRandom};
use self::ring::error::Unspecified;

pub struct AesStream {
    sealing_key: SealingKey,
    opening_key: OpeningKey,
}

impl AesStream {
    pub fn new() -> AesStream {
        //let opening_key =
        let alg = &AES_256_GCM;

        let mut key_bytes = [0x0; 32];
        let random = SystemRandom::new();
        random.fill(&mut key_bytes).unwrap();

        let sealing_key = SealingKey::new(alg, &key_bytes).expect("unable to get sealing_key");
        let opening_key = OpeningKey::new(alg, &key_bytes).expect("unable to get opening_key");

        AesStream {
            sealing_key,
            opening_key,
        }
    }

    pub fn encrypt_block(&mut self, block: &[u8]) -> Result<Vec<u8>, Unspecified> {
        let suffix_len = self.sealing_key.algorithm().tag_len() + MAX_TAG_LEN;
        let new_size = block.len() + suffix_len;

        // TODO nonce
        let nonce = [42, 42, 42, 42,
                    42, 42, 42, 42,
                    42, 42, 42, 42,];
        let ad = [];

        let mut in_out = Vec::with_capacity(new_size);
        in_out.extend_from_slice(block);
        // fill up the buffer with numbers
        in_out.extend((0..suffix_len).map(|x| x as u8));
        assert_eq!(new_size, in_out.len(), "encrypt_block len == new_size");

        let len = seal_in_place(&self.sealing_key, &nonce, &ad, &mut in_out, suffix_len)?;
        assert_eq!(len, in_out.len(), "encrypt_block len");

        Ok(in_out)
    }

    pub fn decrypt_block(&mut self, input: &[u8]) -> Result<Vec<u8>, Unspecified> {
        let in_prefix_len = self.opening_key.algorithm().tag_len();

        // TODO nonce
        let nonce = [42, 42, 42, 42, 42, 42, 42, 42];
        let ad = [];

        let mut in_out = Vec::with_capacity(input.len());
        in_out.extend_from_slice(input);

        let len = {
            let plain_text = open_in_place(&self.opening_key, &nonce, &ad, in_prefix_len, &mut in_out)?;

            plain_text.len()
        };
        in_out.truncate(len);
        assert_eq!(len, in_out.len(), "decrypt_block len");

        Ok(in_out)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    #[ignore]
    fn block64() {
        let block = [42u8; 32];
        let mut aes = AesStream::new();

        let encrypted = aes.encrypt_block(&block).expect("encrypt fail");
        let decrypted = aes.decrypt_block(&encrypted).expect("decrypt fail");

        assert_eq!(block, &*decrypted, "main test");
    }
}
